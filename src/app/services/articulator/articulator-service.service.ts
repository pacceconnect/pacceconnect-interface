import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateArticulatorDto } from 'src/app/module/articulator/create-articulator-dto';

@Injectable({
  providedIn: 'root'
})
export class ArticulatorServiceService {

  public emitEvent = new EventEmitter();
  public url: string = 'http://localhost:44300';

  constructor(private http: HttpClient) { }

  public signup(createArticulatorDto: any): Observable<CreateArticulatorDto>{
    return this.http.post<CreateArticulatorDto>(`${this.url}/articulator/signup`, createArticulatorDto)
      .pipe(
        res => res,
        err => err
      );
  }

  public getArticulatorById(id: number){
    return this.http.get(`${this.url}/articulator/${id}`);
  }
}
