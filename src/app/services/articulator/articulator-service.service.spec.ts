import { TestBed } from '@angular/core/testing';

import { ArticulatorServiceService } from './articulator-service.service';

describe('ArticulatorServiceService', () => {
  let service: ArticulatorServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArticulatorServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
