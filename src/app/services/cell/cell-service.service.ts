import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cell } from 'src/app/module/cell/cell';
import { CellDto } from 'src/app/module/cell/implementation/cellDto';

@Injectable({
  providedIn: 'root'
})
export class CellServiceService {

  private url: string = 'http://localhost:44300';
  private headers: HttpHeaders;
  private token = '';

  constructor(private http: HttpClient) {
    this.setToken();
    this.headers = new HttpHeaders().set('Authorization', `Bearer ${this.token}`)
  }

  public getCellByArticulatorId(articulatorId: any) {

    return this.http.get<Cell>(`${this.url}/cell/${articulatorId}`, { headers: this.headers })
      .pipe(
        res => res,
        err => err
      );
  }

  public createCell(cell: any) : Observable<Cell> {

    return this.http.post<Cell>(`${this.url}/cell`, cell, { headers: this.headers })
      .pipe(
        res => res,
        err => err
      );
  }

  public getAllCells() : Observable<Cell[]> {

    return this.http.get<Cell[]>(`${this.url}/cell/GetAll`, { headers: this.headers })
      .pipe(
        res => res,
        err => err
      );
  }

  public UpdateCell(Cell: any) : Observable<CellDto> {
  
    return this.http.put<CellDto>(`${this.url}/cell`, Cell, { headers: this.headers })
      .pipe(
        res => res,
        err => err
      );
  }

  public UpdateStatusCell(id: number, action: string) {

    return this.http.put(`${this.url}/cell/status/${id}/action/${action}`, null, { headers: this.headers })
      .pipe(
        res => res,
        err => err
      );
  }

  private setToken() {
    let auth = localStorage.getItem('auth');
    if (auth != null) {
      let authJson = JSON.parse(auth);
      this.token = authJson.token;
    }
  }
}
