import { TestBed } from '@angular/core/testing';

import { CellServiceService } from './cell-service.service';

describe('CellServiceService', () => {
  let service: CellServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CellServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
