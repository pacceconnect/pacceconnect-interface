import { Injectable } from '@angular/core';
import { Breadcrumbs } from 'src/app/module/breadcrumbs/Breadcrumbs';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbsServiceService {

  breadcrumbs: Array<Breadcrumbs> = [];

  constructor() { }

  addBreadcrumb(title: string, url: string) {

    if (this.breadcrumbs.length === 0) {
      this.breadcrumbs.push({ title, url });
      return;
    }
    
    this.breadcrumbs.forEach((breadcrumb) => {
      console.log(breadcrumb);
      console.log(title);
      console.log(url);
      if (breadcrumb.title === title) {
        return;
      }
      this.breadcrumbs.push({ title, url });
    });
  }

  removeBreadcrumb() {
    this.breadcrumbs.pop();
  }

  clearBreadcrumbs() {
    this.breadcrumbs = [];
  }
}
