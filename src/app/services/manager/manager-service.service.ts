import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateManagerDto } from 'src/app/module/manager/create-manager-dto';


@Injectable({
  providedIn: 'root'
})
export class ManagerServiceService {

  public emitEvent = new EventEmitter();
  public url: string = 'http://localhost:44300';
  
  constructor(private http: HttpClient) { }

  public signup(createManagerDto: any): Observable<CreateManagerDto> {
    return this.http.post<CreateManagerDto>(`${this.url}/manager/signup`, createManagerDto)
    .pipe(
      res => res,
      err => err
    );
  }
}
