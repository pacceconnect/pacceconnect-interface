import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Auth } from 'src/app/module/authentication/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationServiceService {

  public url: string = 'http://localhost:44300';

  constructor(private http: HttpClient) { }

  public login(login: any): Observable<Auth>{
    console.log(this.url);
    return this.http.post<Auth>(`${this.url}/Auth/authenticate`, login)
      .pipe(
        res => res,
        err => err
      );
  }
}
