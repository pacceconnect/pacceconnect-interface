import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Notification } from 'src/app/module/notification/notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  public url: string = 'http://localhost:44300';
  
  constructor(private http: HttpClient) { }

  public get(userId: number): Observable<Array<Notification>> {

    return this.http.get<Array<Notification>>(`${this.url}/Notification/${userId}`).pipe(
      res => res,
      err => err
    );
  }

  public readNotification(id: number): Observable<Notification> {
    return this.http.put<Notification>(`${this.url}/Notification/${id}/read`, null).pipe(
      res => res,
      err => err
    );
  }
}
