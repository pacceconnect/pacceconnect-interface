import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.scss']
})
export class DrawerComponent implements OnInit, OnChanges {

  constructor() {}

  ngOnInit(): void {
    this.addDarkMode();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['isOpen']) {
      setTimeout(() => {
        this.showDrawer = this.isOpen;
      }, 300);
    }
  }

  @Input() isOpen = false;
  @Input() width: number = 400;
  @Input() position: 'left' | 'right' = 'right';
  @Output() drawerClosed  = new EventEmitter();

  public showDrawer: boolean = false;
  
  close() {
    this.drawerClosed.emit();
  }

  public addDarkMode(): string {
    let classTheme: string = '';
    localStorage.getItem('isDark') == 'true' ? classTheme = 'dark-theme' : classTheme = '';
    console.log(classTheme);
    return classTheme;
  }


  get drawerStyles() {
    const commonStyles = { width: `${this.width}px` };
    if (this.position == 'right') {
      return {
        ...commonStyles,
        right: `${this.isOpen ? 0 : -1 * this.width}px`,
      };
    } else {
      return {
        ...commonStyles,
        left: `${this.isOpen ? 0 : -1 * this.width}px`,
      };
    }
  }
}
