import { Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Notification } from 'src/app/module/notification/notification';
import { NotificationService } from 'src/app/services/notification/notification.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnChanges {

  @Input() public userId: number = 0;
  public notificationsNoRead: Notification[] = [];
  public allNotifications: Notification[] = [];

  public isDrawerOpen: boolean = false;
  public horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  public verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(private _notificationService: NotificationService) { }
  
  ngOnChanges(): void {
    this._notificationService.get(this.userId).subscribe(res => {
        this.allNotifications = res;
        this.notificationsNoRead = res.filter(x => x.isRead == false);
        console.log(res);
      }
    );
  }

  public markAsRead(id: number): void {
    this._notificationService.readNotification(id).subscribe(res => {
        this.ngOnChanges();
      }
    );
  }

  public setReadClass(isRead: boolean): string {
    return isRead ? 'read' : '';
  }

  openDrawer() {
    this.isDrawerOpen = true;
  }

  close() {
    this.isDrawerOpen = false;
  }

  public translateStatus(status: string): string{
    switch (status) {
      case 'Submited':
        return 'Submetido';
      case 'Reviewed':
        return 'Revisado';
      case 'Corrected':
        return 'Corrigido';
      case 'Active':
        return 'Ativo';
      case 'Closed':
        return 'Fechado';
      case 'Canceled':
        return 'Cancelado';
      default:
        return 'Criado';
    }
  }
}
