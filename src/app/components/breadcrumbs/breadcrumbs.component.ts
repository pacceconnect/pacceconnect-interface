import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Breadcrumbs } from 'src/app/module/breadcrumbs/Breadcrumbs';
import { BreadcrumbsServiceService } from 'src/app/services/breadcrumbs/breadcrumbs-service.service';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit, OnDestroy {
  breadcrumbs: Array<Breadcrumbs> = [];

  constructor(
    private breadcrumbsService: BreadcrumbsServiceService,
    private router: Router) {}

  ngOnDestroy(): void {
    this.breadcrumbsService.clearBreadcrumbs();
  }

  ngOnInit(): void {
    this.breadcrumbs = this.breadcrumbsService.breadcrumbs;
  }

  navigateTo(url: string) {
    this.router.navigate([url]);
  }
}
