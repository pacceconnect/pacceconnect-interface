import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Cell } from 'src/app/module/cell/cell';
import { HelpersFunctions } from 'src/app/shared/helpers/helpers-functions';

@Component({
  selector: 'app-cell-card',
  templateUrl: './cell-card.component.html',
  styleUrls: ['./cell-card.component.scss']
})
export class CellCardComponent {

  @Input() cell: Cell | null = null;
  @Output() openDrawerEvent: EventEmitter<any> = new EventEmitter();

  openDrawer() {
    this.openDrawerEvent.emit();
  }

  constructor() { }

  public translateStatusHelper(status: string | undefined): string {
    if (status == null) {
      return '';
    }

    return HelpersFunctions.translateStatus(status);
  }

  public addDarkMode(): string {
    let classTheme: string = '';
    localStorage.getItem('isDark') == 'true' ? classTheme = 'dark-theme' : classTheme = '';
    console.log(classTheme);
    return classTheme;
  }
}
