import { Component, Input, OnChanges } from '@angular/core';
import { ChartOptions, Color } from 'chart.js';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnChanges{
  title = 'ng2-charts-demo';
  @Input() labels: Array<string> = [];
  @Input() data: Array<number> = [];

  // Pie
  public pieChartOptions: ChartOptions<'doughnut'> = {
    responsive: false,
  };
  public pieChartLabels = this.labels;
  public pieChartDatasets = [ {
    data: this.data,
    backgroundColor: [ '#fa1c1c', '#00af0f' ]
  } ];
  public pieChartLegend = true;
  public pieChartPlugins = [];

  constructor() {
  }

  ngOnChanges(): void {
    this.pieChartLabels = this.labels;
    this.pieChartDatasets = [ {
      data: this.data,
      backgroundColor: [ '#fa1c1c', '#00af0f' ]
    } ];
  }
}
