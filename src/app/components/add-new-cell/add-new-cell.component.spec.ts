import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewCellComponent } from './add-new-cell.component';

describe('AddNewCellComponent', () => {
  let component: AddNewCellComponent;
  let fixture: ComponentFixture<AddNewCellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddNewCellComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddNewCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
