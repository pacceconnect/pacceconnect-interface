import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-add-new-cell',
  templateUrl: './add-new-cell.component.html',
  styleUrls: ['./add-new-cell.component.scss']
})
export class AddNewCellComponent implements OnInit, OnChanges{

    @Input() public isDisabled : boolean = false;
    @Output() public createNewCellEvent: EventEmitter<any> = new EventEmitter<any>();

    public class: string = this.isDisabled ? 'disabled-class' : '';
    public isBtnDisabled: boolean = this.isDisabled;

    constructor() { }

    ngOnChanges(changes: SimpleChanges): void {
        this.class = this.isDisabled ? 'disabled-class' : '';
        this.isBtnDisabled = this.isDisabled;
    }

    ngOnInit(): void {
        console.log(this.isDisabled);
    }

    public createNewCell() {
        this.createNewCellEvent.emit();
    }
}
