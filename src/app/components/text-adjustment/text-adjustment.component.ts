import { Component, OnChanges, OnInit } from '@angular/core';
import { timer } from 'rxjs';

@Component({
  selector: 'app-text-adjustment',
  templateUrl: './text-adjustment.component.html',
  styleUrls: ['./text-adjustment.component.scss']
})
export class TextAdjustmentComponent implements OnInit{

  private fontSizeElements: NodeListOf<HTMLElement> | undefined;
  private originalFontSize: Array<number> = [];
  private tags = 'p, h1, h2, h3, h4, h5, h6, span, a, button, label, select, textarea, th, td';

  constructor() {
  }

  ngOnInit(): void {
    timer(1000).subscribe(() => {
      this.getFontSizeElements();
    });
  }

  private getFontSizeElements() {
    this.fontSizeElements = document.querySelectorAll<HTMLElement>(this.tags);

    this.fontSizeElements?.forEach(element => {
      this.originalFontSize.push(parseInt(getComputedStyle(element).fontSize));
    });
  }

  public increaseFontSize() {
    let elements = document.querySelectorAll<HTMLElement>(this.tags);

    elements.forEach(element => {
      let fontSize = parseInt(getComputedStyle(element).fontSize);

      if (fontSize < 30) {
        element.style.fontSize = `${fontSize + 2}px`;
      }
    }) ;
  }

  public decreaseFontSize() {
    let elements = document.querySelectorAll<HTMLElement>(this.tags);

    elements.forEach(element => {
      let fontSize = parseInt(getComputedStyle(element).fontSize);

      if (fontSize > 10) {
        element.style.fontSize = `${fontSize - 2}px`;
      }
    });
  }

  public resetFontSize() {

    let elements = document.querySelectorAll<HTMLElement>(this.tags);

    for (let i = 0; i < elements.length; i++) {
      elements[i].style.fontSize = `${this.originalFontSize[i]}px`;
    }
  }

  public changeContrast() {
    var element = document.body;
    var toolbar = document.querySelector<HTMLElement>('mat-toolbar-row');
    var loginHeader = document.getElementById('header-login');
    var loginMain = document.getElementById('login-content');
    var drawer = document.getElementById('drawer');
    var cellCard = document.getElementById('cell-card');
    var notification_itens = document.querySelectorAll<HTMLElement>('.notification-item');

    if (!element.classList.contains('dark-theme')) {
      element.classList.add('dark-theme');
      toolbar?.classList.add('dark-theme');
      loginHeader?.classList.add('dark-theme');
      loginMain?.classList.add('dark-theme');
      drawer?.classList.add('dark-theme');
      cellCard?.classList.add('dark-theme');
      for (let i = 0; i < notification_itens.length; i++) {
        notification_itens[i].classList.add('notifications-dark');
      }
    }
    else {
      element.classList.remove('dark-theme');
      toolbar?.classList.remove('dark-theme');
      loginHeader?.classList.remove('dark-theme');
      loginMain?.classList.remove('dark-theme');
      drawer?.classList.remove('dark-theme');
      cellCard?.classList.remove('dark-theme');
      for (let i = 0; i < notification_itens.length; i++) {
        notification_itens[i].classList.remove('notifications-dark');
      }
    }

    localStorage.setItem('isDark', element.classList.contains('dark-theme') ? 'true' : 'false');
  }
}
