import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextAdjustmentComponent } from './text-adjustment.component';

describe('TextAdjustmentComponent', () => {
  let component: TextAdjustmentComponent;
  let fixture: ComponentFixture<TextAdjustmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextAdjustmentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TextAdjustmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
