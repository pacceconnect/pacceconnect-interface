import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-count-cell-squad',
  templateUrl: './count-cell-squad.component.html',
  styleUrls: ['./count-cell-squad.component.scss']
})
export class CountCellSquadComponent {
  @Input() public count: number = 0;
}
