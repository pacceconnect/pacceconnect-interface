import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountCellSquadComponent } from './count-cell-squad.component';

describe('CountCellSquadComponent', () => {
  let component: CountCellSquadComponent;
  let fixture: ComponentFixture<CountCellSquadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountCellSquadComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CountCellSquadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
