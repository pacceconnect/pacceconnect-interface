export interface Auth {
    userId: number;
    userName: string;
    role: string;
    token: string;
}
