export class Notification {
    id: number = 0;
    cellName: string = '';
    cellStatus: string = '';
    createdAt: Date = new Date();
    isRead: boolean = false;
}