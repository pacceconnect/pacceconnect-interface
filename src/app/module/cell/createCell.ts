import { CellPlan } from "./cellPlan";
import { CellPlanDto } from "./implementation/cellPlanDto";

export class CreatePlanDto {
    title: string | null = null;
    date: string = "";
    local: string = "";
    time: number = 0;
    duration: number = 0;
    mode: string = "";
    synopsis: string = "";
    justification: string = "";
    targetAudience: string = "";
    activities: string = "";
    tools: string = "";
    resultIndicators: string = "";
    meansOfVerification: string = "";
}

export class CreateCellDto {
    name: string | null = null;
    articulatorId: number = 0;
    plan: CreateCellDto = new CreateCellDto();
}