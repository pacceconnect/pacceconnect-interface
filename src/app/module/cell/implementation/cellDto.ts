import { Cell } from "../cell";
import { CellPlanDto } from "./cellPlanDto";

export class CellDto implements Cell {
    id: number = 0;
    name: string = "";
    status: string = "";
    plan: CellPlanDto = new CellPlanDto();
    articulator: any = null;
}