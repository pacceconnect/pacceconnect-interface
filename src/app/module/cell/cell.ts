import { ArticulatorDto } from "../articulator/articulator-dto";
import { CellPlan } from "./cellPlan";

export interface Cell {
    id: number;
    name: string;
    status: string;
    plan: CellPlan;
    articulator: ArticulatorDto;
}
