export interface CreateArticulatorDto {
    name: string;
    surName: string;
    email: string;
    password: string;
    course: number;
    matriculation: number;
}
