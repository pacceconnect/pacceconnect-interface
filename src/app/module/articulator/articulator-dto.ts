export interface ArticulatorDto {
    id: number,
    name: string,
    surName: string,
    userName: string,
    email: string,
    phoneNumber: string,
    course: string,
    matriculation: 0
}
