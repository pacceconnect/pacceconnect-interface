export interface CreateManagerDto {
    fullName: string
    phoneNumber: number,
    userName: string,
    email: string,
    password: string
}
