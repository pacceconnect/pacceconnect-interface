import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WecolmePageComponent } from './wecolme-page.component';

describe('WecolmePageComponent', () => {
  let component: WecolmePageComponent;
  let fixture: ComponentFixture<WecolmePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WecolmePageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WecolmePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
