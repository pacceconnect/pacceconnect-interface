import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome-header',
  templateUrl: './welcome-header.component.html',
  styleUrls: ['./welcome-header.component.scss']
})

export class WelcomeHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onLoginClick(): void {
    console.log('Login clicked');
  }

  onRegisterClick(): void {
    console.log('Register clicked');
  }
}
