import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthenticationServiceService } from 'src/app/services/authentication/authentication-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-main',
  templateUrl: './login-main.component.html',
  styleUrls: ['./login-main.component.scss']
})
export class LoginMainComponent implements OnInit{
  userNameOrEmailControl = new FormControl('', [Validators.required]);
  passwordControl = new FormControl('', [Validators.required]);
  
  login = this._formBuilder.group({
    emailOrUserName: this.userNameOrEmailControl,
    password: this.passwordControl
  });

  constructor(
    private _formBuilder: FormBuilder,
    private _authenticationService: AuthenticationServiceService,
    private router: Router) { }

  @Output() public sendRegisterSolicitation = new EventEmitter();

  ngOnInit(): void {}

  getUserNameOrEmailErrorMessage(){
    if(this.userNameOrEmailControl.hasError('required')){
      return 'Precisa inserir um nome de usuário ou email';
    }
    return undefined; // Valor padrão de retorno
  }

  getPasswordErrorMessage(){
    if(this.passwordControl.hasError('required')){
      return 'Precisa inserir uma senha';
    }
    return undefined; // Valor padrão de retorno
  }

  public submitForm(){
    if(this.login.valid){
      this._authenticationService.login(this.login.value).subscribe({
        next: (res) => {
          localStorage.setItem('auth', JSON.stringify(res));
          alert('Login realizado com sucesso ' + res.userName);
          if (res.role == 'Articulator'){
            console.log('Articulator');
            this.router.navigate(['/dashboard/your-cells']);
          } else {
            this.router.navigate(['/dashboard/solicitations']);
          }
        },
        error: (err) => {
          if (err.status == 401){
            alert('Usuário ou senha incorretos');
          }
        }
      });
    }
  }

  public registerHandler(){
    this.sendRegisterSolicitation.emit(1);
  }

}
