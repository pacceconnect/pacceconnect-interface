import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ArticulatorServiceService } from 'src/app/services/articulator/articulator-service.service';

@Component({
  selector: 'app-register-articulator-form',
  templateUrl: './register-articulator-form.component.html',
  styleUrls: ['./register-articulator-form.component.scss']
})
export class RegisterArticulatorFormComponent implements OnInit{
  @Output() public sendRegisterSolicitation = new EventEmitter();

  constructor(
    private _formBuilder: FormBuilder,
    private _articulatorService: ArticulatorServiceService) { }

  ngOnInit(): void {}
  
  public nameControl = new FormControl('', [Validators.required]);
  public surNameControl = new FormControl('', [Validators.required]);
  public userNameControl = new FormControl('', [Validators.required]);
  public emailControl = new FormControl('', [Validators.required, Validators.email]);
  public phoneControl = new FormControl('', [Validators.pattern("^[0-9]*$"), Validators.minLength(11), Validators.maxLength(11)]);
  public courseControl = new FormControl('', [Validators.required]);
  public matriculationControl = new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]);
  public password1Control = new FormControl('', [Validators.required, Validators.minLength(8)]);
  public password2Control = new FormControl('', [Validators.required, Validators.minLength(8), (control) => {
    if (control.value !== this.password1Control.value) {
      this.password1Control.setErrors({ passwordsDontMatch: true });
      return { passwordsDontMatch: true };
    }
    this.password1Control.setErrors(null);
    return null;
  }]);

  public hide1 = true;
  public hide2 = true;

  public registerFormArticulator = this._formBuilder.group({
    name: this.nameControl,
    surName: this.surNameControl,
    userName: this.userNameControl,
    email: this.emailControl,
    phone: this.phoneControl,
    course: this.courseControl,
    matriculation: this.matriculationControl,
    password: this.password1Control,
    password2: this.password2Control,
  });

  public getNameErrorMessage(): string {
    if (this.nameControl.hasError('required')) {
      return 'Você precisa inserir um nome';
    }
    return '';
  }

  public getSurNameErrorMessage(): string {
    if (this.surNameControl.hasError('required')) {
      return 'Você precisa inserir um sobrenome';
    }
    return '';
  }

  public getUserNameErrorMessage(): string {
    if (this.userNameControl.hasError('required')) {
      return 'Você precisa inserir um nome de usuário';
    }
    return '';
  }

  public getEmailErrorMessage(): string {
    if (this.emailControl.hasError('required')) {
      return 'Você precisa inserir um email';
    }
    if (this.emailControl.hasError('email')) {
      return 'Você precisa inserir um email válido';
    }
    return '';
  }

  public getPhoneErrorMessage(): string {
    if (this.phoneControl.hasError('pattern')) {
      return 'Você precisa inserir um telefone válido';
    }
    if (this.phoneControl.hasError('minlength')) {
      return 'Você precisa inserir um telefone com 11 dígitos';
    }
    if (this.phoneControl.hasError('maxlength')) {
      return 'Você precisa inserir um telefone com 11 dígitos';
    }
    return '';
  }

  public getCourseErrorMessage(): string {
    if (this.courseControl.hasError('required')) {
      return 'Você precisa inserir um curso';
    }
    return '';
  }

  public getMatriculationErrorMessage(): string {
    if (this.matriculationControl.hasError('required')) {
      return 'Você precisa inserir uma matrícula';
    }
    if (this.matriculationControl.hasError('pattern')) {
      return 'Você precisa inserir uma matrícula válida';
    }
    return '';
  }

  public getPassword1ErrorMessage(): string {
    if (this.password1Control.hasError('required')) {
      return 'Você precisa inserir uma senha';
    }
    if (this.password1Control.hasError('minlength')) {
      return 'Sua senha precisa ter no mínimo 8 caracteres';
    }
    if (this.password2Control.hasError('passwordsDontMatch')) {
      return 'As senhas precisam ser iguais';
    }
    return '';
  }

  public getPassword2ErrorMessage(): string {
    if (this.password2Control.hasError('required')) {
      return 'Você deve inserir uma senha';
    }
    if (this.password2Control.hasError('minlength')) {
      return 'Sua senha deve ter no mínimo 8 caracteres';
    }
    if (this.password2Control.hasError('passwordsDontMatch')) {
      return 'As senhas precisam ser iguais';
    }
    return '';
  }

  public submitForm(): void{
    if (this.registerFormArticulator.valid) {
      var createArticulatorDto = {
        name: this.registerFormArticulator.value.name,
        surName: this.registerFormArticulator.value.surName,
        userName: this.registerFormArticulator.value.userName,
        email: this.registerFormArticulator.value.email,
        phone: this.registerFormArticulator.value.phone,
        course: parseInt(this.registerFormArticulator.value.course as string),
        matriculation: parseInt(this.registerFormArticulator.value.matriculation as string),
        password: this.registerFormArticulator.value.password,
      }
      this._articulatorService.signup(createArticulatorDto).subscribe({
        next: (res) => {
          console.log(res);
          alert("Cadastro realizado com sucesso! Faça login com suas credenciais");
          this.sendRegisterSolicitation.emit(0);
        },
        error: (err) => {
          console.log(err.error.errors);
          console.log(err.error.errors.ErrorCode);
          if (err.error.errors.ErrorCode[0] === "ARTICULATOR_EMAIL_ALREADY_EXISTS") {
            alert("Email já cadastrado, tente outro");
          }
          if (err.error.errors.ErrorCode[0] === "ARTICULATOR_USERNAME_ALREADY_EXISTS") {
            console.log(err.error.errors.ErrorCode);
            alert("Nome de usuário já cadastrado, tente outro");
          }
        }
      });
    }
  }

  public registerHanlder(): void{
    this.sendRegisterSolicitation.emit(0);
  }
}
