import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterArticulatorFormComponent } from './register-articulator-form.component';

describe('RegisterArticulatorFormComponent', () => {
  let component: RegisterArticulatorFormComponent;
  let fixture: ComponentFixture<RegisterArticulatorFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterArticulatorFormComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegisterArticulatorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
