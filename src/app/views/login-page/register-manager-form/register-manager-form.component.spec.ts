import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterManagerFormComponent } from './register-manager-form.component';

describe('RegisterManagerFormComponent', () => {
  let component: RegisterManagerFormComponent;
  let fixture: ComponentFixture<RegisterManagerFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterManagerFormComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegisterManagerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
