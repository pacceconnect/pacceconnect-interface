import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ManagerServiceService } from 'src/app/services/manager/manager-service.service';

@Component({
  selector: 'app-register-manager-form',
  templateUrl: './register-manager-form.component.html',
  styleUrls: ['./register-manager-form.component.scss']
})
export class RegisterManagerFormComponent implements OnInit{

  @Output() public sendRegisterSolicitation = new EventEmitter();

  constructor(
    private _formBuilder: FormBuilder,
    private _managerService: ManagerServiceService) { }
  
  ngOnInit(): void {}

  public fullNameControl= new FormControl('', [Validators.required]);
  public userNameControl= new FormControl('', [Validators.required]);
  public emailControl= new FormControl('', [Validators.required, Validators.email]);
  public phoneControl= new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(11), Validators.maxLength(11)]); 
  public password1Control= new FormControl('', [Validators.required, Validators.minLength(8)]);
  public password2Control= new FormControl('', [Validators.required, Validators.minLength(8), (control) => {
    if (control.value !== this.password1Control.value) {
      this.password1Control.setErrors({ passwordsDontMatch: true });
      return { passwordsDontMatch: true };
      }
      this.password1Control.setErrors(null);
      return null;
    }]);

  public hide1 = true;
  public hide2 = true;
  
  public registerFormManager = this._formBuilder.group({
    fullName: this.fullNameControl,
    userName: this.userNameControl,
    phone: this.phoneControl,
    email: this.emailControl,
    password1: this.password1Control,
    password2: this.password2Control,
  });

  public getFullNameErrorMessage(): string {
    if (this.fullNameControl.hasError('required')) {
      return 'Você deve inserir um nome';
    }
    return '';
  }
  public getUserNameErrorMessage(): string {
    if (this.userNameControl.hasError('required')) {
      return 'Você deve inserir um nome de usuário';
    }
    return '';
  }
  public getPhoneErrorMessage(): string {
    if (this.phoneControl.hasError('required')) {
      return 'Você deve inserir um número';
    }
    if (this.phoneControl.hasError('pattern')) {
      return 'Você deve inserir um número válido';
    }
    if (this.phoneControl.hasError('minlength')) {
      return 'Você deve inserir um número com 11 digitos';
    }
    if (this.phoneControl.hasError('maxlength')) {
      return 'Você deve inserir um número com 11 digitos';
    }
    return '';
  }
  public getEmailErrorMessage(): string {
    if (this.emailControl.hasError('required')) {
      return 'Você deve inserir um email';
    }
    if (this.emailControl.hasError('email')) {
      return 'Você deve inserir um email válido';
    }
    return '';
  }
  public getPassword1ErrorMessage(): string {
    if (this.password1Control.hasError('required')) {
      return 'Você deve inserir uma senha';
    }
    if (this.password1Control.hasError('minlenght')) {
      return 'Você deve inserir uma senha com 8 digitos'
    }
    if (this.password1Control.hasError('passwordsDontMatch')) {
      return 'As senhas não coincidem';
    }
    return '';
  }
  public getPassword2ErrorMessage(): string {
    if (this.password2Control.hasError('required')) {
      return 'Você deve inserir uma senha';
    }
    if (this.password1Control.hasError('minlenght')) {
      return 'Você deve inserir uma senha com 8 digitos'
    }
    if (this.password2Control.hasError('passwordsDontMatch')) {
      return 'As senhas não coincidem';
    }
    return '';
  }

  public submitForm(): void{
    if (this.registerFormManager.valid) {
      var createManagerDto = {
        fullName: this.registerFormManager.value.fullName,
        phoneNumber: this.registerFormManager.value.phone,
        userName: this.registerFormManager.value.userName,
        email: this.registerFormManager.value.email,
        password: this.registerFormManager.value.password1
      }
      this._managerService.signup(createManagerDto).subscribe({
        next: (res) => {
          console.log(res);
          alert("Cadastro realizado com sucesso! Faça login com suas credenciais");
          this.sendRegisterSolicitation.emit(0);
        },
        error: (err) => {
          if (err.error.errors.ErrorCode[0] === "MANAGER_EMAIL_ALREADY_EXISTS") {
            alert("Email já cadastrado, tente outro");
          }
          if (err.error.errors.ErrorCode[0] === "MANAGER_USERNAME_ALREADY_EXISTS") {
            alert("Nome de usuário já cadastrado, tente outro");
          }
        }
      });
    }
  }

  public registerHanlder(): void{
    this.sendRegisterSolicitation.emit(0);
  }
}
