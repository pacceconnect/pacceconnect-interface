import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit{

  ngOnInit(): void {
  }

  constructor(){
  }

  public selectedTab = 0;

  public changeTab(tab: number){
    this.selectedTab = tab;
  }
  
}
