import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterChoiceRoleComponent } from './register-choice-role.component';

describe('RegisterChoiceRoleComponent', () => {
  let component: RegisterChoiceRoleComponent;
  let fixture: ComponentFixture<RegisterChoiceRoleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterChoiceRoleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegisterChoiceRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
