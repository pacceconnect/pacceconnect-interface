import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-register-choice-role',
  templateUrl: './register-choice-role.component.html',
  styleUrls: ['./register-choice-role.component.scss']
})
export class RegisterChoiceRoleComponent implements OnInit{
  
    @Output() public sendRegisterSolicitation = new EventEmitter();

    constructor() {
      this.selectedTab = 0;
    }

    ngOnInit(): void {
      
    }

    public setRule(role: number){
      this.selectedTab = role;
      this.isSelectedRole = true;
    }

    public registerHanlder() {
      this.sendRegisterSolicitation.emit(this.selectedTab);
    }
    public registerHanlderLogin() {
      this.sendRegisterSolicitation.emit(this.login);
    }

    public selectedTab: number = 0;
    public isSelectedRole: boolean = false ;

    public login = 0;
    public manager = 2;
    public articulator = 3;
}
