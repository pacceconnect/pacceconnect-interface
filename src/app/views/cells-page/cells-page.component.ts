import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { take } from 'rxjs';
import { Cell } from 'src/app/module/cell/cell';
import { StatisticsDocumentations } from 'src/app/module/statistics-documentation/statistics-documentation';
import { BreadcrumbsServiceService } from 'src/app/services/breadcrumbs/breadcrumbs-service.service';
import { CellServiceService } from 'src/app/services/cell/cell-service.service';
import { HelpersFunctions } from 'src/app/shared/helpers/helpers-functions';

@Component({
  selector: 'app-cells-page',
  templateUrl: './cells-page.component.html',
  styleUrls: ['./cells-page.component.scss']
})
export class CellsPageComponent implements OnInit{

  public userId: number = 0;
  public cell: Cell | null = null;
  public hasCell: boolean = false;
  public isDrawerOpen: boolean = false;
  public statistics: StatisticsDocumentations = new StatisticsDocumentations();

  public horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  public verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private breadcrumbsService: BreadcrumbsServiceService,
    private _cellService: CellServiceService,
    private _snackBar: MatSnackBar
  ) {
    this.userId = HelpersFunctions.setUserId();
  }
  

  ngOnInit(): void {
    this.breadcrumbsService.addBreadcrumb('Sua célula', '/dashboard/your-cells');

    this._cellService.getCellByArticulatorId(this.userId).pipe(take(1)).subscribe({
      next: (cell) => {
        this.cell = cell;
        this.hasCell = true;
        this.statistics = HelpersFunctions.getQtdPendingAndAproveds(cell);
      }
    });

    if (localStorage.getItem('hasCell') == 'true'){
      this.hasCell = true;
    }
  }

  openDrawer() {
    this.isDrawerOpen = true;
  }

  close() {
    this.isDrawerOpen = false;
  }

  public createNewCell() {
    this.hasCell = true;
    localStorage.setItem('hasCell', 'true');
    this.openSnackBar();
  }

  public openSnackBar() {
    this._snackBar.open('Rascunho salvo com sucesso!', 'Fechar', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: 5000,
      panelClass: ['blue-snackbar']
    });
  }

  public translateStatusHelper(status: string | undefined): string {
    if (status == null) {
      return '';
    }

    return HelpersFunctions.translateStatus(status);
  }
}
