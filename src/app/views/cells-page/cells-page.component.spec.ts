import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CellsPageComponent } from './cells-page.component';

describe('CellsPageComponent', () => {
  let component: CellsPageComponent;
  let fixture: ComponentFixture<CellsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CellsPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CellsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
