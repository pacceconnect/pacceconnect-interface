import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CellPlanTabComponent } from './shared/components/dashboard/articulator-tab/cell-plan-tab/cell-plan-tab.component';
import { DashboardComponent } from './shared/components/dashboard/dashboard.component';
import { SolicitationsComponent } from './shared/components/dashboard/manager-tab/solicitations/solicitations.component';
import {
  NavBarComponentComponent,
} from './shared/components/dashboard/subcomponents/nav-bar-component/nav-bar-component.component';
import { StatisticalSquaresComponent } from './shared/components/statistical-squares/statistical-squares.component';
import { LoginHeaderComponent } from './views/login-page/login-header/login-header.component';
import { LoginMainComponent } from './views/login-page/login-main/login-main.component';
import { LoginPageComponent } from './views/login-page/login-page.component';
import {
  RegisterArticulatorFormComponent,
} from './views/login-page/register-articulator-form/register-articulator-form.component';
import { RegisterChoiceRoleComponent } from './views/login-page/register-choice-role/register-choice-role.component';
import { RegisterManagerFormComponent } from './views/login-page/register-manager-form/register-manager-form.component';
import { WelcomeHeaderComponent } from './views/welcome-page/components/welcome-header/welcome-header.component';
import { WelcomeMainComponent } from './views/welcome-page/components/welcome-main/welcome-main.component';
import { WecolmePageComponent } from './views/welcome-page/wecolme-page.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import { FeedbackRequestComponent } from './shared/components/dashboard/manager-tab/feedback-request/feedback-request.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FeedbackDialogComponent } from './shared/components/dashboard/manager-tab/feedback-request/subcomponents/feedback-dialog/feedback-dialog.component';
import { FormsModule } from '@angular/forms';
import { TextAdjustmentComponent } from './components/text-adjustment/text-adjustment.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { NotificationComponent } from './components/notification/notification.component';
import { CellsPageComponent } from './views/cells-page/cells-page.component';
import { AddNewCellComponent } from './components/add-new-cell/add-new-cell.component';
import { CountCellSquadComponent } from './components/count-cell-squad/count-cell-squad.component';
import {MatCardModule} from '@angular/material/card';
import { CellCardComponent } from './components/cell-card/cell-card.component';
import { DrawerComponent } from './components/drawer/drawer.component';
import { NgChartsModule } from 'ng2-charts';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    AppComponent,
    WecolmePageComponent,
    WelcomeHeaderComponent,
    WelcomeMainComponent,
    LoginPageComponent,
    LoginHeaderComponent,
    LoginMainComponent,
    RegisterChoiceRoleComponent,
    RegisterManagerFormComponent,
    RegisterArticulatorFormComponent,
    DashboardComponent,
    NavBarComponentComponent,
    CellPlanTabComponent,
    SolicitationsComponent,
    StatisticalSquaresComponent,
    FeedbackRequestComponent,
    FeedbackDialogComponent,
    TextAdjustmentComponent,
    BreadcrumbsComponent,
    NotificationComponent,
    CellsPageComponent,
    AddNewCellComponent,
    CountCellSquadComponent,
    CellCardComponent,
    DrawerComponent,
    PieChartComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatGridListModule,
    FlexLayoutModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    HttpClientModule,
    MatMenuModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatStepperModule,
    MatBadgeModule,
    MatTooltipModule,
    CommonModule,
    MatPaginatorModule,
    MatDialogModule,
    FormsModule,
    MatCardModule,
    NgChartsModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
