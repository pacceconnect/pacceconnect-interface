import { Cell } from "src/app/module/cell/cell";
import { StatisticsDocumentations } from "src/app/module/statistics-documentation/statistics-documentation";

export class HelpersFunctions {

  public  static setUserId(): number {
    let auth = localStorage.getItem('auth');
  
    if (auth != null) {
      let authJson = JSON.parse(auth);
      return authJson.userId;
    }
    return 0;
  }

  public static getQtdPendingAndAproveds(cell: Cell): StatisticsDocumentations {
    let statistics = new StatisticsDocumentations();

    var listOfValidated = [
      cell.plan.activitiesComment,
      cell.plan.dayOfWeekComment,
      cell.plan.timeComment,
      cell.plan.durationComment,
      cell.plan.justificationComment,
      cell.plan.localComment,
      cell.plan.meansOfVerificationComment,
      cell.plan.modeComment,
      cell.plan.resultIndicatorsComment,
      cell.plan.synopsisComment,
      cell.plan.targetAudienceComment,
      cell.plan.titleComment,
      cell.plan.toolsComment];
    
      listOfValidated.forEach((value) => {
        if (value === '' || value === null){
          statistics.qtdApproved++;
        } else {
          statistics.qtdPending++;
        }
      });

      return statistics;
  }

  public static translateStatus(status: string): string{
    switch (status) {
      case 'Submited':
        return 'Submetido';
      case 'Reviewed':
        return 'Revisado';
      case 'Corrected':
        return 'Corrigido';
      case 'Active':
        return 'Ativo';
      case 'Closed':
        return 'Fechado';
      case 'Canceled':
        return 'Cancelado';
      default:
        return 'Criado';
    }
  }
}