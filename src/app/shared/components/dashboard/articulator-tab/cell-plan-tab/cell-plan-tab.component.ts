import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { CellDto } from 'src/app/module/cell/implementation/cellDto';
import { CellServiceService } from 'src/app/services/cell/cell-service.service';
import { take } from 'rxjs/operators';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import { BreadcrumbsServiceService } from 'src/app/services/breadcrumbs/breadcrumbs-service.service';
import { HelpersFunctions } from 'src/app/shared/helpers/helpers-functions';

@Component({
  selector: 'app-cell-plan-tab',
  templateUrl: './cell-plan-tab.component.html',
  styleUrls: ['./cell-plan-tab.component.scss'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: {showError: true},
    },
  ],
})

export class CellPlanTabComponent implements OnInit, OnDestroy {

  public userId: number = 0;
  public cell: CellDto = new CellDto();
  public status: string | undefined = 'Criando';
  public isSent: boolean = false;
  public action: string = 'Criar';
  public actionSent: string  = 'Submit';
  public qtdPending: number = 0;
  public qtdApproved: number = 0;
  public hasComments: any = {
    hasTitleComment: false,
    hasDayOfWeekComment: false,
    hasTimeComment: false,
    hasLocalComment: false,
    hasDurationComment: false,
    hasModeComment: false,
    hasSynopsisComment: false,
    hasJustificationComment: false,
    hasTargetAudienceComment: false,
    hasActivitiesComment: false,
    hasToolsComment: false,
    hasResultIndicatorsComment: false,
    hasMeansOfVerificationComment: false,
  };

  public titleControl = new FormControl('', [Validators.required]);
  public dayOfWeekControl = new FormControl('', [Validators.required]);
  public localControl = new FormControl('', [Validators.required]);
  public timeControl = new FormControl('', [Validators.required])
  public durationControl = new FormControl('', [Validators.required]);
  public modeControl = new FormControl('', [Validators.required]);
  public synopsisControl = new FormControl('', [Validators.required]);
  public justificationControl = new FormControl('', [Validators.required]);
  public targetAudienceControl = new FormControl('', [Validators.required]);
  public activitiesControl = new FormControl('', [Validators.required]);
  public toolsControl = new FormControl('', [Validators.required]);
  public resultIndicatorsControl = new FormControl('', [Validators.required]);
  public meansOfVerificationControl = new FormControl('', [Validators.required]);

  public hide1 = true;
  public hide2 = true;

  constructor(
    private _formBuilder: FormBuilder,
    private _cellService: CellServiceService,
    private breadcrumbsService: BreadcrumbsServiceService) {
      this.userId = HelpersFunctions.setUserId();
    }


  ngOnDestroy(): void {
    this.breadcrumbsService.removeBreadcrumb();
  }

  ngOnInit() {

    this.breadcrumbsService.addBreadcrumb(`Plano de célula`, '/dashboard/cell-plan');
    
    this._cellService.getCellByArticulatorId(this.userId).pipe(
      take(1)
    ).subscribe({
      next: (res) => {
        this.cell = res;
        this.titleControl.setValue(this.cell.plan.title);
        this.dayOfWeekControl.setValue(this.cell.plan.dayOfWeek);
        this.localControl.setValue(this.cell.plan.local);
        this.timeControl.setValue(this.cell.plan.time);
        this.durationControl.setValue(this.cell.plan.duration);
        this.modeControl.setValue(this.cell.plan.mode);
        this.synopsisControl.setValue(this.cell.plan.synopsis);
        this.justificationControl.setValue(this.cell.plan.justification);
        this.targetAudienceControl.setValue(this.cell.plan.targetAudience);
        this.activitiesControl.setValue(this.cell.plan.activities);
        this.toolsControl.setValue(this.cell.plan.tools);
        this.resultIndicatorsControl.setValue(this.cell.plan.resultIndicators);
        this.meansOfVerificationControl.setValue(this.cell.plan.meansOfVerification);
        this.setQtdPendingAndAproveds();
        this.setStatus();
        this.setComments();
       
      },
      error: (err) => {
        console.log(err);
      }
    });
  }

  public step1Form = this._formBuilder.group({
    title: this.titleControl,
    dayOfWeek: this.dayOfWeekControl,
    local: this.localControl,
    time: this.timeControl,
    duration: this.durationControl,
  });

  public step2Form = this._formBuilder.group({
    mode: this.modeControl,
    synopsis: this.synopsisControl,
    justification: this.justificationControl,
    targetAudience: this.targetAudienceControl,
    activities: this.activitiesControl,
    tools: this.toolsControl,
    resultIndicators: this.resultIndicatorsControl,
    meansOfVerification: this.meansOfVerificationControl,
  });

  public getTitleErrorMessage(): string {
    if (this.titleControl.hasError('required')) {
      return 'Você precisa inserir um título para a célula';
    }
    return '';
  }

  public getLocalErrorMessage(): string {
    if (this.localControl.hasError('required')) {
      return 'Você precisa inserir um local para a célula';
    }
    return '';
  }

  public getDayOfWeekErrorMessage(): string {
    if (this.dayOfWeekControl.hasError('required')) {
      return 'Você precisa inserir um dia da semana';
    }
    return '';
  }

  public getTimeErrorMessage(): string {
    if (this.timeControl.hasError('required')) {
      return 'Você precisa inserir um horário';
    }
    return '';
  }

  public getDurationErrorMessage(): string {
    if (this.durationControl.hasError('required')) {
      return 'Você precisa inserir uma duração';
    }
    return '';
  }

  public getModeErrorMessage(): string {
    if (this.modeControl.hasError('required')) {
      return 'Você precisa inserir um modo';
    }
    return '';
  }

  public getSynopsisErrorMessage(): string {
    if (this.synopsisControl.hasError('required')) {
      return 'Você precisa inserir uma sinopse';
    }
    return '';
  }

  public getJustificationErrorMessage(): string {
    if (this.justificationControl.hasError('required')) {
      return 'Você precisa inserir uma justificativa';
    }
    return '';
  }

  public getTargetAudienceErrorMessage(): string {
    if (this.targetAudienceControl.hasError('required')) {
      return 'Você precisa inserir um público alvo';
    }
    return '';
  }

  public getActivitiesErrorMessage(): string {
    if (this.activitiesControl.hasError('required')) {
      return 'Você precisa inserir uma atividade';
    }
    return '';
  }

  public getToolsErrorMessage(): string {
    if (this.toolsControl.hasError('required')) {
      return 'Você precisa inserir uma ferramenta';
    }
    return '';
  }

  public getResultIndicatorsErrorMessage(): string {
    if (this.resultIndicatorsControl.hasError('required')) {
      return 'Você precisa inserir um indicador de resultado';
    }
    return '';
  }

  public getMeansOfVerificationErrorMessage(): string {
    if (this.meansOfVerificationControl.hasError('required')) {
      return 'Você precisa inserir um meio de verificação';
    }
    return '';
  }

  public submitForm(): void{
    if (this.step1Form.valid && this.step2Form.valid) {

      let createCellDto = {
        id: this.cell.id,
        name: this.titleControl.value,
        articulatorId: this.userId,
        plan: {
          id: this.cell.plan.id,
          title: this.titleControl.value,
          titleComment: this.cell.plan.titleComment,
          dayOfWeek: this.dayOfWeekControl.value,
          dayOfWeekComment: this.cell.plan.dayOfWeekComment,
          time: this.formatTime(this.timeControl.value),
          timeComment: this.cell.plan.timeComment,
          local: this.localControl.value,
          localComment: this.cell.plan.localComment,
          duration: this.formatTime(this.durationControl.value),
          durationComment: this.cell.plan.durationComment,
          mode: this.modeControl.value,
          modeComment: this.cell.plan.modeComment,
          synopsis: this.synopsisControl.value,
          synopsisComment: this.cell.plan.synopsisComment,
          justification: this.justificationControl.value,
          justificationComment: this.cell.plan.justificationComment,
          targetAudience: this.targetAudienceControl.value,
          targetAudienceComment: this.cell.plan.targetAudienceComment,
          activities: this.activitiesControl.value,
          activitiesComment: this.cell.plan.activitiesComment,
          tools: this.toolsControl.value,
          toolsComment: this.cell.plan.toolsComment,
          resultIndicators: this.resultIndicatorsControl.value,
          resultIndicatorsComment: this.cell.plan.resultIndicatorsComment,
          meansOfVerification: this.meansOfVerificationControl.value,
          meansOfVerificationComment: this.cell.plan.meansOfVerificationComment,
        }
      }

      if (this.status === 'Criando') {
        this._cellService.createCell(createCellDto).subscribe({
          next: (res) => {
            alert('Célula criada com sucesso');
            window.location.href = '/dashboard/cell-plan';
          },
          error: (err) => {
            console.log(err);
          }
        });
      } else {
        console.log(createCellDto);
        this._cellService.UpdateCell(createCellDto).subscribe({
          next: (res) => {
            console.log(res);
            alert('Célula atualizada com sucesso');
            window.location.href = '/dashboard/cell-plan';
          },
          error: (err) => {
            console.log(err);
          }
        });
      }
    }
  }

  public UpdateStatusCell(): void {
    this._cellService.UpdateStatusCell(this.cell.id, this.actionSent).subscribe({
      next: (res) => {
        console.log(res);
        alert('Submissão realizada com sucesso');
        window.location.href = '/dashboard/cell-plan';
      },
      error: (err) => {
        console.log(err);
      }
    });
  }

  public nextStep(stepper: { next: () => void; }) {
    stepper.next();
  }

  private setQtdPendingAndAproveds(): void{
    this.qtdPending = 0;
    this.qtdApproved = 0;
    var listOfValidated = [
      this.cell.plan.activitiesComment,
      this.cell.plan.dayOfWeekComment,
      this.cell.plan.timeComment,
      this.cell.plan.durationComment,
      this.cell.plan.justificationComment,
      this.cell.plan.localComment,
      this.cell.plan.meansOfVerificationComment,
      this.cell.plan.modeComment,
      this.cell.plan.resultIndicatorsComment,
      this.cell.plan.synopsisComment,
      this.cell.plan.targetAudienceComment,
      this.cell.plan.titleComment,
      this.cell.plan.toolsComment];
    
      listOfValidated.forEach((value) => {
      if (value === '' || value === null){
        this.qtdApproved++;
      } else {
        this.qtdPending++;
      }
    });
  }

  private setStatus() {
    let statusOriginal = this.cell.status;
    console.log(statusOriginal);
    if (statusOriginal === 'Created') {
      this.status = 'Criado';
      this.action = 'Submissão'
      this.actionSent = 'Submit'
      this.isSent = true;
    } else if (statusOriginal === 'Submeted') {
      this.status = 'Submetido';
      this.action = ''
      this.isSent = false;
    } else if (statusOriginal === 'Reviewed') {
      this.status = 'Revisado';
      this.action = 'Correção'
      this.actionSent = 'Correct'
      this.isSent = true;
    } else if (statusOriginal === 'Corrected') {
      this.status = 'Corrigido';
      this.action = ''
      this.isSent = false;
    } else if ('Active') {
      this.status = 'Ativo';
      this.action = ''
      this.isSent = false;
    } else if ('Closed') {
      this.status = 'Fechado';
      this.action = ''
      this.isSent = false;
    }
    else if ('Canceled') {
      this.status = 'Cancelado';
      this.action = ''
      this.isSent = false;
    }
  }

  

  private setComments() {
    this.hasComments = {
      hasTitleComment: this.cell.plan.titleComment !== '',
      hasDateComment: this.cell.plan.dayOfWeekComment !== '',
      hasTimeComment: this.cell.plan.timeComment !== '',
      hasDayOfWeekComment: this.cell.plan.dayOfWeekComment !== '',
      hasLocalComment: this.cell.plan.localComment !== '',
      hasDurationComment: this.cell.plan.durationComment !== '',
      hasModeComment: this.cell.plan.modeComment !== '',
      hasSynopsisComment: this.cell.plan.synopsisComment !== '',
      hasJustificationComment: this.cell.plan.justificationComment !== '',
      hasTargetAudienceComment: this.cell.plan.targetAudienceComment !== '',
      hasActivitiesComment: this.cell.plan.activitiesComment !== '',
      hasToolsComment: this.cell.plan.toolsComment !== '',
      hasResultIndicatorsComment: this.cell.plan.resultIndicatorsComment !== '',
      hasMeansOfVerificationComment: this.cell.plan.meansOfVerificationComment !== '',
    }

    console.log(this.hasComments);
  }

  private formatTime(time: string | null): string {
    if (time === null) {
      return '';
    }
    if (time.split(':').length === 2) {
      return time + ':00';
    }
    return time;
  }
}