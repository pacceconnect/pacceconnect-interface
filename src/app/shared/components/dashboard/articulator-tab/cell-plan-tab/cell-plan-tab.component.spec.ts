import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CellPlanTabComponent } from './cell-plan-tab.component';

describe('CellPlanTabComponent', () => {
  let component: CellPlanTabComponent;
  let fixture: ComponentFixture<CellPlanTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CellPlanTabComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CellPlanTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
