import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-nav-bar-component',
  templateUrl: './nav-bar-component.component.html',
  styleUrls: ['./nav-bar-component.component.scss']
})
export class NavBarComponentComponent {

  @Input() public userName: string = '';
  @Input() public userId: number = 0;
}
