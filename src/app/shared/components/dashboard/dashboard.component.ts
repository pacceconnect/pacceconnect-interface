import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BreadcrumbsServiceService } from 'src/app/services/breadcrumbs/breadcrumbs-service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{

  public userId: number = 0;
  public userName: string = '';
  public userRole: number = 0;

  constructor(
    private breadcrumbsService: BreadcrumbsServiceService,
    private router: Router) {}

  ngOnInit(): void {

    var auth = localStorage.getItem('auth');

    if (auth != null){
      var authJson = JSON.parse(auth);
      this.userId = authJson.userId;
      this.userName = authJson.userName;
      this.userRole = this.setRole(authJson.role);
      console.log(this.userId + ' ' + this.userName + ' ' + this.userRole);
    }
  }

  public setRole(role: string): number{
    if (role === "Articulator") {
      return 2;
    }
    return 1;
  }

  public logout(){
    localStorage.removeItem('auth');
    this.router.navigate(['/login']);
  }
}
