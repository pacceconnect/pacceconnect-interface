import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { CellDto } from 'src/app/module/cell/implementation/cellDto';
import { CellServiceService } from 'src/app/services/cell/cell-service.service';
import { FeedbackDialogComponent } from './subcomponents/feedback-dialog/feedback-dialog.component';
import { BreadcrumbsServiceService } from 'src/app/services/breadcrumbs/breadcrumbs-service.service';

export interface DialogData {
  item: string;
  description: string;
  feedback: string;
}

@Component({
  selector: 'app-feedback-request',
  templateUrl: './feedback-request.component.html',
  styleUrls: ['./feedback-request.component.scss']
})

export class FeedbackRequestComponent implements OnInit, OnDestroy{

  public feedback: string = '';

  public cellId: number = 0;
  public cell: CellDto = new CellDto();
  public titleValidateControl = new FormControl(false, [Validators.required]);
  public dayOfWeekValidateControl = new FormControl(false, [Validators.required]);
  public timeValidateControl = new FormControl(false, [Validators.required]);
  public durationValidateControl = new FormControl(false, [Validators.required]);
  public justificationValidateControl = new FormControl(false, [Validators.required]);
  public localValidateControl = new FormControl(false, [Validators.required]);
  public meansOfVerificationValidateControl = new FormControl(false, [Validators.required]);
  public modeValidateControl = new FormControl(false, [Validators.required]);
  public resultIndicatorsValidateControl = new FormControl(false, [Validators.required])
  public synopsisValidateControl = new FormControl(false, [Validators.required]);
  public targetAudienceValidateControl = new FormControl(false, [Validators.required]);
  public toolsValidateControl = new FormControl(false, [Validators.required]);
  public activitiesValidateControl = new FormControl(false, [Validators.required]);
  public timeFormated: string = '';
  public dayOfWeekFormated: string = '';
  
  public isSent: boolean = false;
  public intention: string = '';
  public status: string = '';
  public action: string = '';
  public qtdPending: number = 0;
  public qtdApproved: number = 0;
  public bla: boolean = true;

  public hasComments: any = {
    hasTitleComment: true,
    hasdayOfWeek: false,
    hasLocalComment: false,
    hasDurationComment: false,
    hasModeComment: false,
    hasSynopsisComment: false,
    hasJustificationComment: false,
    hasTargetAudienceComment: false,
    hasActivitiesComment: false,
    hasToolsComment: false,
    hasResultIndicatorsComment: false,
    hasMeansOfVerificationComment: false,
  };

  public feedbackFormStep1 = this._formBuilder.group({
    titleComment: this.titleValidateControl,
    dayOfWeek: this.dayOfWeekValidateControl,
    durationComment: this.durationValidateControl,
    localComment: this.localValidateControl,
  });

  public getErrorMessage(): string {
    console.log('entrou');
    return 'Campo obrigatório';
  }

  public feedbackFormStep2 = this._formBuilder.group({
    justificationComment: this.justificationValidateControl,
    meansOfVerificationComment: this.meansOfVerificationValidateControl,
    modeComment: this.modeValidateControl,
    resultIndicatorsComment: this.resultIndicatorsValidateControl,
    synopsisComment: this.synopsisValidateControl,
    targetAudienceComment: this.targetAudienceValidateControl,
    toolsComment: this.toolsValidateControl,
    activitiesComment: this.activitiesValidateControl,
  });
  
  constructor(
    private _formBuilder: FormBuilder,
    private __cellService: CellServiceService,
    public dialog: MatDialog,
    private breadcrumbsService: BreadcrumbsServiceService,
  ) {
    this.cellId = parseInt(window.location.pathname.split('/')[3]);
  }

  ngOnInit(): void {

    this.breadcrumbsService.addBreadcrumb(`Feedback - ${this.cellId}`, '/dashboard/feedback-request/' + this.cellId);

    this.__cellService.getCellByArticulatorId(this.cellId).subscribe({
      next: (res) => {
        this.cell = res;
        this.setQtdPendingAndAproveds();
        this.setStatus();
        this.setDayOfWeek();
      }
    });
  }

  ngOnDestroy(): void {
    this.breadcrumbsService.removeBreadcrumb();
  }

  public nextStep(stepper: { next: () => void; }) {
    stepper.next();
  }

  public openDialogTitle(description: string): void {
    if (this.cell.plan.titleComment !== '') {
      this.feedback = this.cell.plan.titleComment;
    }

    const dialogRef = this.dialog.open(FeedbackDialogComponent, {
      
      data: {item: 'Título', description: description, feedback: this.feedback},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.feedback = result;

      if (result === undefined) {
        this.feedback = '';
      } else {
        this.cell.plan.titleComment = this.feedback;
        console.log(this.cell.plan.titleComment);
      } 
      this.feedback = '';
      this.setQtdPendingAndAproveds();
      this.setStatus()
    });
  }

  public openDialogDayOfWeek(description: string): void {
    if (this.cell.plan.dayOfWeekComment !== '') {
      this.feedback = this.cell.plan.dayOfWeekComment;
    }

    const dialogRef = this.dialog.open(FeedbackDialogComponent, {
      data: {item: 'Data e Hora', description: description, feedback: this.feedback},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.feedback = result;

      if (result === undefined) {
        this.feedback = '';
      } else {
        this.cell.plan.dayOfWeekComment = this.feedback;
        console.log(this.cell.plan.dayOfWeekComment);
      } 
      this.feedback = '';
      this.setQtdPendingAndAproveds();
      this.setStatus()
    });
  }

  public openDialogTime(description: string): void {
    if (this.cell.plan.timeComment !== '') {
      this.feedback = this.cell.plan.timeComment;
    }

    const dialogRef = this.dialog.open(FeedbackDialogComponent, {
      data: {item: 'Horário', description: description, feedback: this.feedback},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.feedback = result;

      if (result === undefined) {
        this.feedback = '';
      } else {
        this.cell.plan.timeComment = this.feedback;
        console.log(this.cell.plan.timeComment);
      }
      this.feedback = '';
      this.setQtdPendingAndAproveds();
      this.setStatus()
    });
  }

  public openDialogDuration(description: string): void {
    if (this.cell.plan.durationComment !== '') {
      this.feedback = this.cell.plan.durationComment;
    }

    const dialogRef = this.dialog.open(FeedbackDialogComponent, {
      data: {item: 'Duração', description: description, feedback: this.feedback},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.feedback = result;

      if (result === undefined) {
        this.feedback = '';
      } else {
        this.cell.plan.durationComment = this.feedback;
        console.log(this.cell.plan.durationComment);
      } 
      this.feedback = '';
      this.setQtdPendingAndAproveds();
      this.setStatus()
    });
  }

  public openDialogLocal(description: string): void {
    if (this.cell.plan.localComment !== '') {
      this.feedback = this.cell.plan.localComment;
    }

    const dialogRef = this.dialog.open(FeedbackDialogComponent, {
      data: {item: 'Local', description: description, feedback: this.feedback},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.feedback = result;

      if (result === undefined) {
        this.feedback = '';
      } else {
        this.cell.plan.localComment = this.feedback;
        console.log(this.cell.plan.localComment);
      } 
      this.feedback = '';
      this.setQtdPendingAndAproveds();
      this.setStatus()
    });
  }

  public openDialogMode(description: string): void {
    if (this.cell.plan.modeComment !== '') {
      this.feedback = this.cell.plan.modeComment;
    }

    const dialogRef = this.dialog.open(FeedbackDialogComponent, {
      data: {item: 'Modo', description: description, feedback: this.feedback},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.feedback = result;

      if (result === undefined) {
        this.feedback = '';
      } else {
        this.cell.plan.modeComment = this.feedback;
        console.log(this.cell.plan.modeComment);
      } 
      this.feedback = '';
      this.setQtdPendingAndAproveds();
      this.setStatus()
    });
  }

  public openDialogJustification(description: string): void {
    if (this.cell.plan.justificationComment !== '') {
      this.feedback = this.cell.plan.justificationComment;
    }

    const dialogRef = this.dialog.open(FeedbackDialogComponent, {
      data: {item: 'Justificativa', description: description, feedback: this.feedback},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.feedback = result;

      if (result === undefined) {
        this.feedback = '';
      } else {
        this.cell.plan.justificationComment = this.feedback;
        console.log(this.cell.plan.justificationComment);
      } 
      this.feedback = '';
      this.setQtdPendingAndAproveds();
      this.setStatus()
    });
  }

  public openDialogSynopsis(description: string): void {
    if (this.cell.plan.synopsisComment !== '') {
      this.feedback = this.cell.plan.synopsisComment;
    }

    const dialogRef = this.dialog.open(FeedbackDialogComponent, {
      data: {item: 'Sinopse', description: description, feedback: this.feedback},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.feedback = result;

      if (result === undefined) {
        this.feedback = '';
      } else {
        this.cell.plan.synopsisComment = this.feedback;
        console.log(this.cell.plan.synopsisComment);
      } 
      this.feedback = '';
      this.setQtdPendingAndAproveds();
      this.setStatus()
    });
  }

  public openDialogTargetAudience(description: string): void {
    if (this.cell.plan.targetAudienceComment !== '') {
      this.feedback = this.cell.plan.targetAudienceComment;
    }

    const dialogRef = this.dialog.open(FeedbackDialogComponent, {
      data: {item: 'Público Alvo', description: description, feedback: this.feedback},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.feedback = result;
      
      if (result === undefined) {
        this.feedback = '';
      } else {
        this.cell.plan.targetAudienceComment = this.feedback;
        console.log(this.cell.plan.targetAudienceComment);
      } 
      this.feedback = '';
      this.setQtdPendingAndAproveds();
      this.setStatus()
    });
  }

  public openDialogActivities(description: string): void {
    if (this.cell.plan.activitiesComment !== '') {
      this.feedback = this.cell.plan.activitiesComment;
    }

    const dialogRef = this.dialog.open(FeedbackDialogComponent, {
      data: {item: 'Atividades', description: description, feedback: this.feedback},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.feedback = result;
      
      if (result === undefined) {
        this.feedback = '';
      } else {
        this.cell.plan.activitiesComment = this.feedback;
        console.log(this.cell.plan.activitiesComment);
      } 
      this.feedback = '';
      this.setQtdPendingAndAproveds();
      this.setStatus()
    });
  }

  public openDialogTools(description: string): void {
    if (this.cell.plan.toolsComment !== '') {
      this.feedback = this.cell.plan.toolsComment;
    }

    const dialogRef = this.dialog.open(FeedbackDialogComponent, {
      data: {item: 'Ferramentas', description: description, feedback: this.feedback},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.feedback = result;
      
      if (result === undefined) {
        this.feedback = '';
      } else {
        this.cell.plan.toolsComment = this.feedback;
        console.log(this.cell.plan.toolsComment);
      } 
      this.feedback = '';
      this.setQtdPendingAndAproveds();
      this.setStatus()
    });
  }

  public openDialogResultIndicators(description: string): void {
    if (this.cell.plan.resultIndicatorsComment !== '') {
      this.feedback = this.cell.plan.resultIndicatorsComment;
    }

    const dialogRef = this.dialog.open(FeedbackDialogComponent, {
      data: {item: 'Indicadores de Resultados', description: description, feedback: this.feedback},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.feedback = result;
      
      if (result === undefined) {
        this.feedback = '';
      } else {
        this.cell.plan.resultIndicatorsComment = this.feedback;
        console.log(this.cell.plan.resultIndicatorsComment);
      } 
      this.feedback = '';
      this.setQtdPendingAndAproveds();
      this.setStatus()
    });
  }

  public openDialogMeansOfVerification(description: string): void {
    if (this.cell.plan.meansOfVerificationComment !== '') {
      this.feedback = this.cell.plan.meansOfVerificationComment;
    }

    const dialogRef = this.dialog.open(FeedbackDialogComponent, {
      data: {item: 'Meios de Verificação', description: description, feedback: this.feedback},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.feedback = result;
      
      if (result === undefined) {
        this.feedback = '';
      } else {
        this.cell.plan.meansOfVerificationComment = this.feedback;
        console.log(this.cell.plan.meansOfVerificationComment);
      } 
      this.feedback = '';
      this.setQtdPendingAndAproveds();
      this.setStatus()
    });
  }

  public submitForm(): void{
    console.log("submit")
    if (this.feedbackFormStep1.valid && this.feedbackFormStep2.valid) {

      let update = {
        id: this.cell.id,
        name: this.cell.name,
        articulatorId: this.cell.articulator.id,
        plan: {
          id: this.cell.plan.id,
          title: this.cell.plan.title,
          titleComment: this.cell.plan.titleComment,
          dayOfWeek: this.cell.plan.dayOfWeek,
          dayOfWeekComment: this.cell.plan.dayOfWeekComment,
          local: this.cell.plan.local,
          localComment: this.cell.plan.localComment,
          time: this.cell.plan.time,
          timeComment: this.cell.plan.timeComment,
          duration: this.cell.plan.duration,
          durationComment: this.cell.plan.durationComment,
          mode: this.cell.plan.mode,
          modeComment: this.cell.plan.modeComment,
          synopsis: this.cell.plan.synopsis,
          synopsisComment: this.cell.plan.synopsisComment,
          justification: this.cell.plan.justification,
          justificationComment: this.cell.plan.justificationComment,
          targetAudience: this.cell.plan.targetAudience,
          targetAudienceComment: this.cell.plan.targetAudienceComment,
          activities: this.cell.plan.activities,
          activitiesComment: this.cell.plan.activitiesComment,
          tools: this.cell.plan.tools,
          toolsComment: this.cell.plan.toolsComment,
          resultIndicators: this.cell.plan.resultIndicators,
          resultIndicatorsComment: this.cell.plan.resultIndicatorsComment,
          meansOfVerification: this.cell.plan.meansOfVerification,
          meansOfVerificationComment: this.cell.plan.meansOfVerificationComment,
        }
      }

      this.__cellService.UpdateCell(update).subscribe({
        next: (res) => {
          this.__cellService.UpdateStatusCell(this.cell.id, this.intention).subscribe({
            next: (res) => {
              alert('Solicitação enviada com sucesso!');
              window.location.reload();
            }
          });
        },
        error: (err) => {
          console.log(err);
        }
      });
    }
  }

  private setStatus() {
    let statusOriginal = this.cell.status;
    console.log(statusOriginal);
    if (statusOriginal === 'Submeted' && this.qtdPending > 0) {
      this.status = 'Submetido';
      this.intention = 'Review';
      this.action = 'Correção'
      this.isSent = true;
    } else if (statusOriginal === 'Submeted' && this.qtdPending === 0) {
      this.status = 'Submetido';
      this.intention = 'Approve';
      this.action = 'Aprovação'
      this.isSent = true;
    } else if (statusOriginal === 'Reviewed') {
      this.status = 'Revisado';
      this.action = ''
      this.isSent = false;
    } else if (statusOriginal === 'Corrected' && this.qtdPending > 0) {
      this.status = 'Corrigido';
      this.action = 'Review'
      this.action = 'Correção'
      this.isSent = true;
    } else if (statusOriginal === 'Corrected' && this.qtdPending === 0) {
      this.status = 'Corrigido';
      this.intention = 'Approve';
      this.action = 'Aprovação'
      this.isSent = true;
    } else if (statusOriginal === 'Active' && this.qtdPending > 0) {
      this.status = 'Ativo';
      this.intention = 'Review';
      this.action = 'Correção'
      this.isSent = true;
    } else if (statusOriginal === 'Active' && this.qtdPending === 0) {
      this.status = 'Ativo';
      this.action = ''
      this.isSent = false;
    } else if ('Closed') {
      this.status = 'Fechado';
      this.action = ''
      this.isSent = false;
    } else if ('Canceled') {
      this.status = 'Cancelado';
      this.action = ''
      this.isSent = false;
    }
  }

  private setQtdPendingAndAproveds(): void{
    this.qtdPending = 0;
    this.qtdApproved = 0;
    var listOfValidated = [
      this.cell.plan.activitiesComment,
      this.cell.plan.dayOfWeekComment,
      this.cell.plan.timeComment,
      this.cell.plan.durationComment,
      this.cell.plan.justificationComment,
      this.cell.plan.localComment,
      this.cell.plan.meansOfVerificationComment,
      this.cell.plan.modeComment,
      this.cell.plan.resultIndicatorsComment,
      this.cell.plan.synopsisComment,
      this.cell.plan.targetAudienceComment,
      this.cell.plan.titleComment,
      this.cell.plan.toolsComment];

      console.log(listOfValidated);
    
      listOfValidated.forEach((value) => {
      if (value === '' || value === null){
        this.qtdApproved++;
      } else {
        this.qtdPending++;
      }
    });
  }

  setDayOfWeek(): void {
    if (this.cell.plan.dayOfWeek === 'Sunday') {
      this.dayOfWeekFormated = 'Domingo'; return;
    }
    if (this.cell.plan.dayOfWeek === 'Monday') {
      this.dayOfWeekFormated = 'Segunda-feira'; return;
    }
    if (this.cell.plan.dayOfWeek === 'Tuesday') {
      this.dayOfWeekFormated = 'Terça-feira'; return;
    }
    if (this.cell.plan.dayOfWeek === 'Wednesday') {
      this.dayOfWeekFormated = 'Quarta-feira'; return;
    }
    if (this.cell.plan.dayOfWeek === 'Thursday') {
      this.dayOfWeekFormated = 'Quinta-feira'; return;
    }
    if (this.cell.plan.dayOfWeek === 'Friday') {
      this.dayOfWeekFormated = 'Sexta-feira'; return;
    }
    if (this.cell.plan.dayOfWeek === 'Saturday') {
      this.dayOfWeekFormated = 'Sábado'; return;
    }
  }
}