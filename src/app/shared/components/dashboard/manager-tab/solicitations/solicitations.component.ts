import { Component, OnDestroy, OnInit } from '@angular/core';
import { CellServiceService } from 'src/app/services/cell/cell-service.service';
import { take } from 'rxjs';
import { Cell } from 'src/app/module/cell/cell';
import { BreadcrumbsServiceService } from 'src/app/services/breadcrumbs/breadcrumbs-service.service';

@Component({
  selector: 'app-solicitations',
  templateUrl: './solicitations.component.html',
  styleUrls: ['./solicitations.component.scss']
})
export class SolicitationsComponent implements OnInit{

  constructor(
    private _cellService: CellServiceService,
    private breadcrumbsService: BreadcrumbsServiceService,
  ) { }

  public solicitations: Cell[] = [];
  public displayedColumns: string[] = ['title', 'articulator', 'status', 'link'];
  public dataSource = this.solicitations;
  public solicitationTotal: number = 0;
  public solicitationPending: number = 0;
  public solicitationaActive: number = 0;

  addBreadcrumb() {
    var hasElement = false;
    this.breadcrumbsService.breadcrumbs.forEach(element => {
      if (element.title === 'Solicitações'){
        hasElement = true;
      }
    });
    if (!hasElement){
      this.breadcrumbsService.addBreadcrumb('Solicitações', '/dashboard/solicitations');
    }
  }

  ngOnInit(): void {
    this.addBreadcrumb();
    
    this._cellService.getAllCells().pipe(
      take(1)
    ).subscribe({
      next: (res) => {
        this.solicitations = res;
        this.dataSource = this.solicitations;
        
        this.setSolicitationTotal();
        this.setSolicitationPending();
        this.setSolicitationActive();	
      }
    });
  }

  private setSolicitationTotal(){
    this.solicitationTotal = this.solicitations.length;
  }

  private setSolicitationPending(){
    this.solicitationPending = this.solicitations
      .filter((solicitation) => solicitation.status !== 'Active').length;
  }

  private setSolicitationActive(){
    this.solicitationaActive = this.solicitations
      .filter((solicitation) => solicitation.status == 'Active').length;
  }

  public translateStatus(status: string): string{
    switch (status) {
      case 'Submited':
        return 'Submetido';
      case 'Reviewed':
        return 'Revisado';
      case 'Corrected':
        return 'Corrigido';
      case 'Active':
        return 'Ativo';
      case 'Closed':
        return 'Fechado';
      case 'Canceled':
        return 'Cancelado';
      default:
        return 'Criado';
    }
  }

  public translateStatusColor(status: string): string{
    switch (status) {
      case 'Submited':
        return 'blue';
      case 'Reviewed':
        return 'yellow';
      case 'Corrected':
        return 'orange';
      case 'Active':
        return 'green';
      case 'Closed':
        return 'red';
      case 'Canceled':
        return 'red';
      default:
        return 'blue';
    }
  }
}
