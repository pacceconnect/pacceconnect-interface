import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-statistical-squares',
  templateUrl: './statistical-squares.component.html',
  styleUrls: ['./statistical-squares.component.scss']
})
export class StatisticalSquaresComponent {
  @Input() public title1: string = '';
  @Input() public prop1: string | number  = '';
  @Input() public title2: string = '';
  @Input() public prop2: string | number  = '';
  @Input() public title3: string = '';
  @Input() public prop3: string | number  = '';
}
