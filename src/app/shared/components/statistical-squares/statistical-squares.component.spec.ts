import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticalSquaresComponent } from './statistical-squares.component';

describe('StatisticalSquaresComponent', () => {
  let component: StatisticalSquaresComponent;
  let fixture: ComponentFixture<StatisticalSquaresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatisticalSquaresComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StatisticalSquaresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
