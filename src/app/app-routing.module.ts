import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WecolmePageComponent } from './views/welcome-page/wecolme-page.component';
import { LoginPageComponent } from './views/login-page/login-page.component';
import { DashboardComponent } from './shared/components/dashboard/dashboard.component';
import { CellPlanTabComponent } from './shared/components/dashboard/articulator-tab/cell-plan-tab/cell-plan-tab.component';
import { SolicitationsComponent } from './shared/components/dashboard/manager-tab/solicitations/solicitations.component';
import { FeedbackRequestComponent } from './shared/components/dashboard/manager-tab/feedback-request/feedback-request.component';
import { CellsPageComponent } from './views/cells-page/cells-page.component';

const routes: Routes = [

  {path: '' , component: WecolmePageComponent },
  {path: 'login', component: LoginPageComponent },
  {path: 'dashboard', component: DashboardComponent, children: [
    {path: 'your-cells', component: CellsPageComponent},
    {path: 'your-cells/cell-plan', component: CellPlanTabComponent},
    {path: 'solicitations', component: SolicitationsComponent},
    {path: 'request/:id', component: FeedbackRequestComponent}
  ]}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes), RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
